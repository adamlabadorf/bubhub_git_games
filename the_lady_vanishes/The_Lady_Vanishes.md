# The Lady Vanishes

This game is basically [hangman](https://en.wikipedia.org/wiki/Hangman_(game))
but doesn't involve overtly killing people. On the game board is a picture of a
woman (stick figure, use your imagination). She's wearing a hat that is,
obviously, the letter `m`:

```
    m
    O
   /|\
    |
   / \
```

The game has two players. One player chooses a word, and the other player's
goal is to guess the word by guessing one letter at a time. The word is displayed
on the board masked as underscores (in this example, the secret chosen word is
*TRAIN*):

```
    m
    O
   /|\
    |
   / \

Word: _ _ _ _ _
```

The guessing player knows, via her cunning intellect, that the word has five
letters in it, and may begin guessing letters, one at a time. Her first guess
might be *A*:

```
    m
    O
   /|\
    |
   / \

Word: _ _ _ _ _

Last letter guessed: A

Letters remaining:
  B C D E F G H I J K L M N O P Q R S T U V W X Y Z
```

Note the clever indication of the last guessed letter and the enormously
helpful display of letters already guessed. The person who chose the word
then reveals where in the word, if anywhere, the guessed letter occurs:

```
    m
    O
   /|\
    |
   / \

Word: _ _ A _ _

Last letter guessed: A

Letters remaining:
  B C D E F G H I J K L M N O P Q R S T U V W X Y Z
```

The guessing player, being a classically trained geologist, immediately
surmises that the secret word is *shale*. She then guesses the letter *S*:

```
    m
    O
   /|\
    |
   / \

Word: _ _ A _ _

Last letter guessed: S

Letters remaining:
  B C D E F G H I J K L M N O P Q R   T U V W X Y Z
```

Since, at the time of the writing of this readme, the letter *S* does not exist
in the word *TRAIN*, the first player responds by mysteriously vanishing the lady's
left leg (she's facing away from the screen, duh):

```
    m
    O
   /|\
    |
     \

Word: _ _ A _ _

Last letter guessed: S

Letters remaining:
  B C D E F G H I J K L M N O P Q R   T U V W X Y Z
```

The lady does not appear to be in any pain or distress, thankfully, despite
her leg being no longer visible, so the guessing player assumes it's not the
lady but her that has the problem. She dutifully continues guessing, until
either she correctly guesses the word, or the lady (and her hat) vanishes
entirely.
