# Dots and Boxes

This game can be played with any number of players. The game starts with a grid
of dots, like:

```
. . . . .
         
. . . . .
         
. . . . .
         
. . . . .
         
. . . . .
```

On each players turn, the player may connect any two dots with a line in either
the vertical or horizontal direction. For instance:

```
._. . . .
         
. . . . .
         
. . . . .
         
. . . . .
         
. . . . .
```


Here the first two dots of the first row have been connected with a line. Players
alternate drawing lines, with the goal of enclosing areas of the grid with lines.
Say a game being played by two players A and B has arrived at the following:

```
._._. . .
|        
. . . . .
|   |    
._._. . .
|   |    
. . . . .
        |
. . . ._.
```

The area in the top left is nearly enclosed as a square, with only one line missing.
It is B's turn, who decides to draw the final line and enclose the square:

```
._._. . .
|B B|    
. . . . .
|B B|    
._._. . .
|   |    
. . . . .
        |
. . . ._.
```

Since B enclosed the square, the cells inside the enclosed area are awarded to player
B, and tracked by the letters written into the grid. The game ends when no more lines
can be drawn, and player with the most enclosed squares wins.
